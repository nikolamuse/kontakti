﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace kontakti.Models
{
    public class ContactDetails
    {
        [Key]
        public int contactID { get; set; }

        [Column(TypeName ="nvarchar(30)")]
        public string contactName { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        public string contactSurname { get; set; }

        [Column(TypeName = "nvarchar(30)")]
        public string contactAdress { get; set; }
    }
}
